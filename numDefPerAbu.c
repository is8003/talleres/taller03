#include <stdio.h>

int main(void){
	for (unsigned int n = 1; n <= 30; n++){	// Para n igual a 1; hasta n menor o igual a 30; icremento n en 1:
		unsigned int divProps = 0;	// Declaro divProps y asigno valor de 0;

		for (unsigned int div = 1; div < n; div++){	// Para div igual a 1; hasta div menor que n; incremento div en 1:
			if (n % div == 0){			// Si n % div es igual a 0:
				divProps += div;		// Suma entre div y divProps, asigno resultado a divProps.
			}
		}

		if(divProps < n){				// Si divProps es menor que n:
			printf("%u es deficiente.\n", n);	// Imprimo n es deficiente.
		}
		else if (divProps > n){				// De lo contrario, si divProps es mayor que n:
			printf("%u es abundante.\n", n);	// Imprimo n es abundante.
		}
		else{						// De lo contrario (divProps es igual a n):
			printf("%u es perfecto.\n", n);		// Imprimo n es perfecto.
		}

	}

}
