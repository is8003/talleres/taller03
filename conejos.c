#include <stdio.h>

int main(void){
	unsigned int dias = 0;		// Declaro dias y asigno valor de 0.
	unsigned int pAyer, pHoy;	// Declaro pAyer y pHoy.
	
	printf("Ingresa población de conejos de ayer: ");	// Pregunto a usuario por población de conejos de ayer.
	scanf("%u", &pAyer);					// Obtengo población de ayer y asigno a variable pAyer.
	printf("Ingresa población de conejos de hoy: ");	// Pregunto a usuario por población de conejos de hoy.
	scanf("%u", &pHoy);					// Obtengo población de hoy y asigno a variable pHoy.

	while(pHoy <= 70000){		// Mientras pHoy sea menor o igual a 70000:
		unsigned int pTemp;	// Declaro pTemp.
		dias++;			// Incremento en 1 a dias.
		pTemp = pHoy;		// Asigno a pTemp el valor de pHoy.
		pHoy += pAyer;		// A pHoy le asigno la suma entre pHoy y pAyer.
		pAyer = pTemp;		// A pAyer le asigno valor de pTemp.
	}

	printf("Días trancurridos antes de exportación de conejos: %u\n", dias);	// Imprimo dias
}
