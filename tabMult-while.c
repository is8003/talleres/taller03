#include <stdio.h>

int main(void){
	int linea = 1;	// Declaro e inicializo linea en 1

	while(linea <= 9){	// Mientras linea menor o igual a 9:
		int mult = 1;	// Declaro e inicializo mult en 1

		while(mult <= linea){			// Mientras mult menor o igual a linea:
			printf("%2d ", mult * linea);	// Imprimo multiplicación entre mult y linea.
			mult++;				// Incremento mult en 1
		}

		puts("");	// Imprimo cambio de línea
		linea++;	// Incremento linea en 1
	}
}
