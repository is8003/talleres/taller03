# 1. Tabla de multiplicación 
Escriba un programa en C para escribir la siguiente tabla de multiplicación:

```text
1
2  4
3  6  9
4  8 12 16
5 10 15 20 25
6 12 18 24 30 36
7 14 21 28 35 42 49
8 16 24 32 40 48 56 64
9 18 27 36 45 54 63 72 81
```

## Refinamiento 1

1. Para las líneas de 1 a 9:
   1. Para múltiplos desde 1 hasta número de línea:
      1. Imprimo resultado de número de línea por múltiplo.
   1. Imprimo cambio de línea

## Refinamiento 2

<div class="center">

```mermaid
graph TD
    A(("Inicio"))
    B(("Fin"))
    C["linea = 1"]
    D["mult = 1"]
    E{"linea <= 9"}
    F{"mult <= linea"}
    G[/"Imprimo: mult * linea"/]
    J[/"Imprimo: nueva línea"/]
    H["mult++"]
    I["linea++"]

    A -->C 
    C -->E  
    E --"Verdadero"-->D 
    E --"Falso"-->B 
    D -->F 
    F --"Verdadero"-->G 
    F --"Falso"-->J 
    J -->I 
    I -->E 
    G -->H
    H -->F
```
</div>

### Usando `for`

1. Para `linea` desde `1`; hasta `linea` menor o igual `9`; incremento `linea` en 1:
   1. Para `mult` desde `1`; hasta `mult` menor o igual a `linea`; incremento
      `mult` en 1:
      1. Imprimo `mult * linea`.
   1. Imprimo cambio de línea

### Usando `while`

1. Declaro e inicializo `linea` en 1
1. Mientras `linea <= 9`:
   1. Declaro e inicializo `mult` en 1
   1. Mientras `mult` sea menor o igual a `linea`:
      1. Imprimo `mult * linea`.
      1. Incremento `mult` en 1
   1. Imprimo cambio de línea
   1. Incremento `linea` en 1

# 2. Conejos

En la población de la isla de Taporu están muy preocupados debido a que tienen
que convivir con conejos y éstos se reproducen rápidamente. Los conejos se
reproducen según la fórmula:

P<sub>t+1</sub> = P<sub>t</sub> + P<sub>t-1</sub>

Es decir, la población de conejos de mañana es igual a la población conejos de
hoy más la de ayer. Ellos se reunieron y tomaron la decisión que cuando dicha
población sobrepase los 70000 van a enviar 40000 conejos a la isla vecina.
Ellos quieren saber, dada la población de ayer y de hoy, en cuánto tiempo
tendrán que realizar la exportación de conejo.

## Refinamiento 1

1. Obtener de usuario población de conejos de ayer y hoy.
1. Mientras población de conejos de hoy sea menor o igual a 70000:
   1. Contar día.
   1. Calcular nueva población de conejos (P<sub>t+1</sub> = P<sub>t</sub> +
      P<sub>t-1</sub>).
1. Imprimir cantidad de días transcurridos.

## Refinamiento 2

<div class="center">

```mermaid
graph TD
    A(("Inicio"))
    B(("Fin"))
    C[/"Obtener población<br>de ayer y hoy"/]
    D{"Población de hoy<br>menor o igual a 70000"}
    E["Contar día"]
    F[["Calcular nueva<br>población"]]
    G[/"Imprimir total<br>de días"/]

    A -->C
    C -->D
    D --"Verdadero"-->E
    E -->F
    F -->D
    D --"Falso"-->G 
    G -->B
```
</div>

1. Pregunto a usuario por población de conejos de ayer.
1. Obtengo población de ayer y asigno a variable `pAyer`.
1. Pregunto a usuario por población de conejos de hoy.
1. Obtengo población de hoy y asigno a variable `pHoy`.
1. Mientras `pHoy <= 70000`:
   1. Incremento en 1 a `dias`.
   1. Asigno a `pTemp` el valor de `pHoy`.
   1. A `pHoy` le asigno la suma entre `pHoy` y `pAyer`.
   1. A `pAyer` le asigno valor de `pTemp`.
1. Imprimo `dias`

## Refinamiento 3

1. Declaro `dias` y asigno valor de 0.
1. Declaro `pAyer` y `pHoy`.
1. Pregunto a usuario por población de conejos de ayer.
1. Obtengo población de ayer y asigno a variable `pAyer`.
1. Pregunto a usuario por población de conejos de hoy.
1. Obtengo población de hoy y asigno a variable `pHoy`.
1. Mientras `pHoy <= 70000`:
   1. Declaro `pTemp`.
   1. Incremento en 1 a `dias`.
   1. Asigno a `pTemp` el valor de `pHoy`.
   1. A `pHoy` le asigno la suma entre `pHoy` y `pAyer`.
   1. A `pAyer` le asigno valor de `pTemp`.
1. Imprimo `dias`

# 3. Números deficientes, perfectos y abundantes

Los divisores propios de un número entero n son los divisores positivos menores
que n. Un entero positivo se dice que es un número deficiente, perfecto o
abundante, si la suma de estos divisores propios es menor que, igual a, o mayor
que el número respectivamente.

Ejemplo: `8` es deficiente porque `1+2+4<8`; `6` es perfecto porque `1+2+3=6`;
`12` es abundante porque `1+2+3+4+6>12`. Elabore un programa que encuentre e
imprima los números deficientes, perfectos y abundantes entre 1 y 30.

## Refinamiento 1

1. Para `n` de 1 a 30:
   1. Para `div` de 1 hasta `div` menor que `n`:
      1. Si `div` es propio de `n`, sumar `div` a `divProps`.
   1. Si `divProps` es menor a `n`:
      1. Imprimo `n` es deficiente.
   1. De lo contrario, si `divProps` es mayor a `n`:
      1. Imprimo `n` es abundante.
   1. De lo contrario (`divProps` es igual a `n`):
      1. Imprimo `n` es perfecto.

## Refinamiento 2

```mermaid
graph TD
    A(("Inicio"))
    B(("Fin"))
    C["n = 1"]
    D["div = 1"]
    K["divProps = 0"]
    E{"n <= 30"}
    F{"div < n"}
    G[/"Imprimo: n es deficiente"/]
    J[/"Imprimo: n es abundante"/]
    L[/"Imprimo: n es Perfecto"/]
    H["div++"]
    I["n++"]
    M{"n % div == 0"}
    N["divProps += div"]
    O{"divProps < n"}
    P{"divProps > n"}

    A -->C 
    C -->E  
    E --"Verdadero"-->D 
    E --"Falso"-->B 
    D -->K
    K -->F
    F --"Verdadero"-->M 
    F --"Falso"-->O
    M --"Verdadero"-->N 
    M --"Falso"-->H
    N -->H
    H -->F 
    O --"Verdadero"-->G
    G -->I 
    I -->E
    O --"Falso"-->P 
    P --"Verdadero"-->J 
    J -->I
    P --"Falso"-->L
    L -->I
```

1. Para `n` desde 1; hasta `n` menor o igual a `30`; incremento `n` en 1:
   1. Para `div` desde 1 ; hasta `div` menor que `n`; incremento `div` en 1:
      1. Si `n % div == 0`(`div` es un divisor propio de `n`):
         1. Suma entre `div` y `divProps`, asigno resultado a `divProps`.
   1. Si `divProps < n`:
      1. Imprimo `n` es deficiente.
   1. De lo contrario, si `divProps > n`:
      1. Imprimo `n` es abundante.
   1. De lo contrario (`divProps == n`):
      1. Imprimo `n` es perfecto.

## Refinamiento 3

1. Para `n` desde 1; hasta `n` menor o igual a `30`; incremento `n` en 1:
   1. Declaro `divProps` y asigno valor de 0;
   1. Para `div` desde 1 ; hasta `div` menor que `n`; incremento `div` en 1:
      1. Si `n % div == 0`(`div` es un divisor propio de `n`):
         1. Suma entre `div` y `divProps`, asigno resultado a `divProps`.
   1. Si `divProps < n`:
      1. Imprimo `n` es deficiente.
   1. De lo contrario, si `divProps > n`:
      1. Imprimo `n` es abundante.
   1. De lo contrario (`divProps == n`):
      1. Imprimo `n` es perfecto.
