#include <stdio.h>

int main(void){
	for (int linea = 1; linea <= 9; linea++){		// Para linea igual a 1; hasta linea menor o igual a 9; incremento linea en 1:
		for (int mult = 1; mult <= linea; mult++){	// Para mult igual a 1; hasta mult menor o igual a linea; incremento mult en 1:
			printf("%2d ", mult * linea);		// Imprimo multiplicación entre mult y linea.
		}

		puts("");	// Imprimo cambio de línea
	}
}
